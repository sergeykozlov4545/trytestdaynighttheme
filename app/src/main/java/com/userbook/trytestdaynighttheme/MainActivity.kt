package com.userbook.trytestdaynighttheme

import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val BUNDLE_TRANSPARENT_STATUS_BAR = "transparent_status_bar"
    }

    private var transparentStatusBar: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        transparentStatusBar = savedInstanceState?.getBoolean(BUNDLE_TRANSPARENT_STATUS_BAR, false) ?: false

        toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.toggle_theme) {
                if (isNight()) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                }
            }
            return@setOnMenuItemClickListener true
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            initStatusBar()
        } else {
            button.isVisible = false
        }
        button.setOnClickListener { toggleStatusBarColor() }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(BUNDLE_TRANSPARENT_STATUS_BAR, transparentStatusBar)
    }

    private fun isNight(): Boolean {
        return resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK ==
                Configuration.UI_MODE_NIGHT_YES
    }

    private fun toggleStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            transparentStatusBar = transparentStatusBar.not()
            initStatusBar()
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun initStatusBar() {
        if (!transparentStatusBar) {
            window.clearFlags(FLAG_TRANSLUCENT_STATUS)
            button.setText(R.string.not_transparent_status_bar)
            return
        }
        window.addFlags(FLAG_TRANSLUCENT_STATUS)
        button.setText(R.string.transparent_status_bar)
    }
}
